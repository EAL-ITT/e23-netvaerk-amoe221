---
title: '23E Netværk AMOE221'
subtitle: 'Studieplan'
filename: 'E23_NETVAERK_AMOE221_lecture_plan'
authors: ['Allan Misasa Nielsen \<amni1@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Allan Misasa Nielsen'
email: 'amni1@ucl.dk'
left-header: \today
right-header: Studieplan
skip-toc: false
semester: 23F
---

# Studieplan

Studieplanen består af følgende dele: en ugeplan, en oversigt over fordelingen af studieaktiviteter og generelle oplysninger om kurset, modulet eller projektet.

* Studieprogram, klasse og semester: IT-teknologi, IToE221+222, 23S
* Undervisers navn og dato for udfyldelse af skemaet: AMNI1 2023-01-31
* Kursets, modulets eller projektets titel og ECTS: Indlejrede Systemer 2, 6 ECTS
* Nødvendig læsning, litteratur eller tekniske anvisninger og andet baggrundsmateriale: Rover kit, 1. semester kit, ESP32, Raspberry Pi.

Se ugeplanen for detaljer som detaljeret dagsplan, links, referencer, øvelser osv

 Underviser |  Uge | Indhold | Ressourcer |
| :---: | :---: | :---: | :---: |
|  AMNI1 | 37 | Introduktion til programmering, datatyper og datastrukturer, programmeringsmiljø | PET C3 og C4 og TP C1 og C2 |
|  AMNI1 | 38 | Loops | PET C5 og TP C7 |
|  Ingen | 39 | Løse opgaver | Ingen |
|  Ingen | 40 | Øvelser | Ingen |
|  AMNI1 | 41 | Funktioner | PET C6 og TP C3 |
|  Ingen | 42 | Løse opgaver | Ingen|
|  AMNI1 | 43 | Lasercut | Ingen |
|  AMNI1 | 44 | Debugging og recap øvelser | PET C8 |
|  AMNI1 | 45 | Objektorienteret programmering 1 | PET C7 TP C15 |
|  AMNI1 | 46 | Loops og funktioner 2, samt counters | Ingen |
|  AMNI1 | 47 | State machines | Ingen |
|  AMNI1 | 48 | Threading og multiprocessering | PET C9 og [Video](https://www.youtube.com/watch?v=AZnGRKFUU0c) |
|  AMNI1 | 49 | Milepæl - Threading og state machines sammen til kontrol | Ingen |
|  AMNI1 | 50 | Recap øvelser samt filhåndtering | PET C10 og TP C14 |
|  Ingen | 51 | Undervisningsfri | Ingen |
|  Ingen | 52 | Glædelig jul! | Ingen |
|  AMNI1 | 01 | Færdiggørelse af rover | Ingen |
|  AMNI1 | 02 | Færdiggørelse af rover samt eksamensforberedelse | Ingen |

## Info om faget

Dette kursus har til formål at bygge flere lag på den studerendes forståelse for basal signalbehandling via sensorer, samt basale protokoller og Python.

Dette mål vil opnås via aktuelle PBL projekter der syntetiserer denne opbyggede viden, samt lag på lag bygger flere moderne IoT-teknikker på, bl.a.:

* Docker til containerization
* Linux som operativsystem
* Lora til trådløs kommunikation
* Moderne dokumentation samt versionsstyring og portfolio via GitLab og Markdown

Kort fortalt, vil den studerende efter dette kursus være klædt på til at bygge IoT-løsninger, med forskellige trådløse protokoller, sensorer, samt visualiseringsløsninger og datalagringsplatforme.

## Læringsmål

### Viden

Den studerende har viden om og forståelse for:

* V1: Kommunikations- og Interfaceteknik generelt samt hvorledes de anvendes i udvalgte løsninger
* V2: Elektroniske moduler i overblik samt hvorledes udvalgte moduler er opbygget
* V3: Protokoller inkl. kommunikationsprotokoller, deres opbygning samt hvori forskelle og anvendelsesmuligheder er
* V4: Internet of Things-teknikker, generelt om opbygningen, og i mere detaljer udvalgte løsninger
* V5: Teknisk matematik anvendt indenfor fagområdet til forståelse af elektronik og/ eller kommunikation
* V6: Operativsystemer, deres særkende og anvendelse
* V7: Signalhåndtering i en generel forståelse samt forstå hvorledes det anvendes og indgår i løsninger

### Færdigheder

Den studerende kan:

* F1: Udvælge, tilpasse og anvende indlejrede systemer og komponenter i sikre og bæredygtige løsninger
* F2: Opbygge og anvende testsystemer
* F3: Dokumentere og formidle opgaver og løsninger med anvendelse af indlejrede komponenter og systemer

### Kompetencer

Den studerende kan:

* K1: Håndtere analyse, behovsafdækning, design, udvikling og test af sikre indlejrede og bæredygtige løsninger
* K2: Håndtere analyse, diagnosticering, test og service af den teknologi, der indgår i arbejdet med elektroniske systemer under hensyntagen til økonomi-, miljø- og kvalitetskrav
* K3: Tilegne sig ny viden, færdigheder og kompetencer indenfor fagområdet

## Metode

* PBL til praktiske opgaver
* Laboratorieøvelser i elektroniklaboratoriet
* Live programmering - til at illustrere teorier og eksempler i et live programmeringsmiljø
* Tværfaglig syntese af uddannelsens fag i større opgaver

## Udstyr

* Raspberry Pi
* ESP32
* Rover kit
* Første semester kit

## Projekter med eksterne partnere

* Roverprojekt med Københavns Erhvervsakademi.

## Prøveform

Dette fag indeholder 1 skriftlig eksamen.

Se semesterbeskrivelsen på [https://www.ucl.dk/studiedokumenter/it-teknolog](https://www.ucl.dk/studiedokumenter/it-teknolog) for detaljer om OLA'er og eksamener.

Du kan læse mere om studerendes kontakt til videngrundlaget, og om vores pædagogiske praksis på følgende link:  https://esdhweb.ucl.dk/D22-1997205.pdf?_ga=2.219881625.1681755584.1663010855-1314688763.1633613668

## Studieaktivitetsmodellen

![Studieaktivitetsmodellen](https://i.imgur.com/KM8ioDa.png)

## Andre generelle informationer

Ingen på nuværende tidspunkt.
