---
title: 'COURSE TEMPLATE'
subtitle: 'Ugentlige planer'
authors: ['Allan Misasa Nielsen \<amni1@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Allan Misasa Nielsen'
date: \today
email: 'amni1@ucl.dk'
left-header: \today
right-header: 'COURSE TEMPLATE, ugentlige planer'
---

Introduktion

====================

This document is a collection of weekly plans. It is based on the wekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programm for each week of the second semester project.

# Uge XX

## Ugens mål

Praktiske og læringsmål er som følger:

### Praktiske mål

* ..
* ..

### Læringsmål

* ..
* ..

## Afleveringer

* ..
* ..

Herunder er det foreløbige skema:

### ITOE231

| Tidspunkt | Aktiviteter |
| :---: | :--- |
| 8:15 | Live coding |
| 9:45 | Pause |
| 10:00 | Øvelser  |
| 11:30 | Frokost |  

### ITOE232

| Tidspunkt | Aktiviteter |
| :---: | :--- |
| 12:15 | Live coding |
| 13:45 | Pause |
| 14:00 | Øvelser  |

## Hands-on tid

### Øvelse 0

![this is an example image](ucl_logo_raw.png)

...

### Øvelse 1

...

### Øvelse 2

...

## Kommentarer

* ..
* ..

## Supplerende materiale

Foreløbigt ingen
