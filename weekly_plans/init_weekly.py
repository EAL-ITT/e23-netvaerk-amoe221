import re

start_week = 36
end_week = 52

weeks = [week for week in range(start_week, end_week + 1)]

for week in weeks:
    with open('weekly_plans/weekly_ww01.md', 'r') as f:
        content = f.read()
        print("Read content from weekly_ww01.md")
        content_new = re.sub('XX', str(week), content)
        new_file = open('weekly_plans/weekly_ww' + str(week) + '.md', 'w')
        new_file.write(content_new)
        new_file.close()
        f.close()