---
Week: 2 Torsdag
Content: Opgave 10. Rutning, en ruter, to subnets. Virtuel ruter
Material: See links in weekly plan
Initials: PDA
---

# Uge 2 Torsdag 11-1-2023

1. K1: Opsamling og feedback på forudgående arbejde.  
1. K1. Underviser introducerer denne eller disse ugers arbejde.  
2. K4. De studerende arbejder med opgaven og spørger underviseren om læringsmål, der kræver yderligere diskussion. 
3. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
4. K3. Studerende arbejder alene med opgavegennemgang og sikrer sig, at alle læringsmål behandles.

* De ovenfor anvendte (KX)-koder henviser til den studieaktivitetsmodel, der er beskrevet i semesterplanen.

   <button onclick="location.href='https://eal-itt.gitlab.io/22a-itt1-netvaerk/22A_ITT1_NETVAERK_lecture_plan.pdf'" type="button"> Semesterplan</button>  

## Læringsmål (LM) og emner der vil blive drøftet på klassen:

Herunder en  oversigt over hvilket læringsmål i studieordningen de emner vi arbejder med hører under.
Læringsmålskoderne (LMXX) henviser til læringsmålene som de er gentaget i semesterplanen.

**Viden:**

Den studerende har opnået viden om og forståelse af:

* (LMV1) Grundlæggende forståelse for en ruters role i et netværk.
* (LMV1) Hvordan man installerer en router i VMware Workstation VMWW.
* (LMV1) Hvilke roller ARP og MAC addresser spille ri rutning.
* (LMV1) Hvordan ruter interface indgår i broadcast domain.
* (LMV2) Hvordan man anvender en CLI på en ruter.
* (LMV2) Ruteren som gateway.

**Færdigheder:**

Den studerende kan:

* (LMF1) Installere en ruter i VMware Workstation.
* (LMF3) Udarbejde dokumentation for installation af ruter i Workstation.

**Kompetencer:**

Den studerende kan:

* (LMK3) konfigurere en ruter til rutning imellem 2 subnets.
* (LMK3) Sætte sig ind i hvordan rutning imellem 2 subnets foregår.
* (LMK4) Sevstændigt og i samarbejde med teammedlemmer stå for processen med at installere en ruter i VMware Workstation VMWW.

## Underviserens undervisningsplan for denne uge:

Underviseren vil tale om følgende emner på klassen:

* Grundlæggende forståelse for en ruters role i et netværk.
* Ruteren som gateway.
* Hvilke roller ARP og MAC addresser spiller i rutning.
* At ruterinterface indgår i broadcast domain.
* Ruter konfiguration.
* Hvordan man installerer en router i VMware Workstation VMWW.
* Hvordan man opnår kontakt med og anvender CLI på en ruter.
* Upload af konfiguration til ruter.


## Dokumentation den studerende skal udarbejde og aflevere:

* Den studerende skal udarbejde dokumentation til:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-10-routing-one-router-two-subnets-virtual-router'" type="button"> Opgave 10</button>  

* Dokumentationen bedes afleveret på ITSL. Dette er afgørende for at kunne få feedback på sin dokumentation og give feedback på andres dokumentationen.

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button>  

## Estimeret tidsforbrug:

* 8 hours.  

## Underviserens instruktionsvideoer:

* Demonstrations, instruktions og teorivideoer

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-10-one-router-two-subnets'" type="button"> Opgave 10 videoer</button> 

## Eksterne videokilder:
* PING Command - Troubleshooting  

  <button onclick="location.href='https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos'" type="button"> Ping</button>  

* ARP og routing. Denne video er med to rutere. Vær opmærksom at ARP og rutning er to helt separate processer.

  <button onclick="location.href='https://www.youtube.com/watch?v=cn8Zxh9bPios'" type="button"> ARP and Routing</button>  

## Underviserens instruktionstekster:

* Forbindelser i netværkslab B2.19:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#netlab/'" type="button"> Netværkslab kabling</button>  

* Networking troubleshooting eller problemløsning:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Problemløsning</button>  

<!--
* 18A VMware Juniper Junos vSRX basic routing PDA V0x.pdf  
* 19A_IP_command_cheatsheet_PDA.pdf  
* Only Section 6.1 in this document: Python_Network_programming_PDA_V08_p8-18.pdf  
* Regarding the CLI, chapter 2 in: JNCIA-IJOS-Junosphere-12.c-R_SG.pdf  


**Configuration(s)**

Please find the base configuration for this assignment here:

<button onclick="location.href='https://perper.gitlab.io/networkingpages/srx_configs/#_top'" type="button"> Configurations</button>  
-->

## Konfiguration(er)

* Eksempel(er) på konfiguration(er) findes her:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/srx_configs/#_top'" type="button"> Configurations</button>  

## Software

* Disse tre filer udgør en virtuel ruter når de installeres på VMWW. Filer findes i Resourcer på ITSL:

      junos-vsrx-12.1X47-D15.4-domestic.mf  
      junos-vsrx-12.1X47-D15.4-domestic.ovf  
      junos-vsrx-12.1X47-D15.4-domestic-disk1.vmdk  