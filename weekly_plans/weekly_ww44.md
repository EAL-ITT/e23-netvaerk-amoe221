---
Week: 2 Onsdag
Content:  Opgave 15. Fysisk netværk med switch.   
Material: See links in weekly plan
Initials: PDA
---

# Uge 2 Onsdag 10-01-2023

## K1 - K4 kronologisk.

1. K1. Fælles gennemgang og feedback på foregående opgave.
2. K1. Underviser introducerer denne eller disse ugers arbejde. 
3. K4. De studerende arbejder med opgaven og spørger underviseren om læringsmål, der kræver yderligere diskussion. 
4. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
5. K3. Studerende arbejder alene med opgavegennemgang og sikrer sig, at alle læringsmål behandles.

## Læringsmål og emner der vil blive drøftet på klassen:

Den studerende har opnået viden om og forståelse af:

* Se i opgaven.  

## Underviserens undervisningsplan for i dag:

* Patch panel i Netværks Lab. 

## Dokumentation den studerende skal udarbejde og aflevere:

* Den studerende skal udarbejde dokumentation til opgave 15 Physical network with switch.:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-15-physical-network-with-switch'" type="button"> Assignment 15</button>  

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button>   

## Underviserens instruktionsvideoer:

* Demonstrations, instruktions og teorivideoer til opgave 15: From network design to implementation in networking lab.

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-15-from-network-design-to-implementation-in-networking-lab'" type="button"> Assignment 15 videoes</button> 
  
## Eksterne videokilder:  

* Ping network testing program for Troubleshooting:  
  [https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos](https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos)  

## Underviserens instruktionstekster:

* Networking troubleshooting eller problemløsning:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>  

* Instruktioner til at forbinde fra bordene til patchpanelet og videre til udstyret i datacenterrummet: 

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#netlab/'" type="button"> Netværks Lab B2.19</button>   

## Eksterne instruktionstekster:

* Linux manualen for ip neighbour kommand:   

  [https://man7.org/linux/man-pages/man8/ip-neighbour.8.html](https://man7.org/linux/man-pages/man8/ip-neighbour.8.html)  

