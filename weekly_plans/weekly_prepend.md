---
title: 'Skabelon'
subtitle: 'Ugentlige planer'
authors: ['Allan Misasa Nielsen \<amni1@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Allan Misasa Nielsen'
date: \today
email: 'amni1@ucl.dk'
left-header: \today
right-header: 'Skabelon, ugentligeplaner'
---

Introduktion

====================

Dette dokument er en samling af ugeplaner. Det er baseret på ugeplanerne i det administrative arkiv og opdateres automatisk ved ændringer.

Afsnittene beskriver mål og program for hver uge i projektet på andet semester.
