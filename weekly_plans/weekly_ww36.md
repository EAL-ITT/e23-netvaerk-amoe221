---
Week: 1 Onsdag
Content:  Opgave 3. VM Ware Workstation og installation af en virtuel Linux-maskine.   
Material: Se links i ugeplanen
Initials: PDA
---

# Uge 1 Onsdag 3-1-2023

## K1 - K4 kronologisk.

1. K1. Underviser introducerer disse ugers arbejde.  
2. K4. De studerende arbejder med opgaven og spørger underviseren om emner, der kræver yderligere diskussion. 
3. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
4. K3. Studerende arbejder alene med opgavegennemgang.

* De ovenfor anvendte (KX)-koder henviser til den studieaktivitetsmodel, der er beskrevet i semesterplanen.

   <button onclick="location.href='https://eal-itt.gitlab.io/e22-netvaerk-amoe211/E22_NETVAERK_AMOE211_lecture_plan.pdf'" type="button"> Semesterplan</button>  

## Læringsmål og emner der vil blive drøftet på klassen:

Vi snakker om de overordnede læringsmål for faget:

**Viden:**

Den uddannede har viden om: 

* Internets opbygning 
* OSI modellen - TCP/IP stackken og 5 lagsmodellen.
* Almindeligt netværksudstyr 

**Færdigheder:**

Den uddannede kan: 

* konfigurere virtuelle maskiner 
* fejlfinde på netværk ud fra OSI modellen 
* sniffe og analyse simpel netværks trafik 

**Kompetencer :**

Den uddannede kan: 

* konfigurere mindre ethernet baserede netværk 

**Metode**

Faget vil være opbygget omkring korte introduktioner, videoer og opgaver, som følges op med QA sessioner og dialog efter behov. 

## Underviserens undervisningsplan:

Underviseren vil tale om følgende emner på klassen:

* Sprog. I IT-verdenen er meget på Engelsk.
* ITSL. Semesterplan og links til materiale på GitLab.
* PeerGrade. E,g, gennemgang og aflevering af deadlines. Hvordan laver vi Peer review af de foregående opgaver?
* Webside for opgaver.
* TimeEdit. Struktur for netværksdage.
* Opgave 3 og 2.
* Formatet for aflevering. Dvs. det indleverede Word-skabelon-dokument.
* Teori/videoer til denne uges opgave. Opgave 3 og 2.

## Dokumentation den studerende skal udarbejde og aflevere:

* Den studerende skal udarbejde dokumentation til:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-3-vmware-workstation-and-a-base-linux-virtual-machine-vm-installation'" type="button"> Opgave 3</button>  

* Dokumentationen bedes afleveret på ITSL. Dette er afgørende for at kunne få feedback på sin dokumentation og give feedback på andres dokumentationen.

* Det er obligatorisk at skrive dokumentationen ind i den MS Word skabelon som ligger på ITSL. Hvis den studerende selv kan udarbejde en bedre struktur er dette selvfølgelig fint. Skabelonen angiver blot minimumskrav.

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button>  

## Estimeret tidsforbrug:

* 8 timer.

## Underviserens instruktionsvideoer:

* Demonstrations, instruktions og teorivideoer til opgave 3. 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-3-vmware-workstation-and-a-base-linux-virtual-machine-vm-installation'" type="button"> Opgave 3 videoer</button>

## Eksterne videokilder:

* Create a Virtual Machine in VMware Workstation Pro Version 12.
A detailed walkthrough on how to install Lubuntu.  

  [https://www.youtube.com/watch?v=BHpRTVP8upg&ab_channel=danscourses](https://www.youtube.com/watch?v=BHpRTVP8upg&ab_channel=danscourses)  


## Underviserens instruktionstekster:

* Networking troubleshooting eller problemløsning:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Problemløsning</button>  

## Software:

* VMware Workstation

  [https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html](https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html)
