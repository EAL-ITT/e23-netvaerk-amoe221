---
Week: 2 Mandag
Content:  Opgave 16. Linux rutningstabel. Default gateway. Grundlæggende om IP-pakken.    
Material: See links in weekly plan
Initials: PDA
---

# Uge 2 Mandag 8-1-2023

## K1 - K4 kronologisk.

1. K1. Sammen gennemgår og giver vi feedback på foregående opgave.
2. K1. Underviser introducerer denne eller disse ugers arbejde.  
3. K4. De studerende arbejder med opgaven og spørger underviseren om læringsmål, der kræver yderligere diskussion. 
4. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
5. K3. Studerende arbejder alene med opgavegennemgang og sikrer sig, at alle læringsmål behandles.

## Læringsmål og emner der vil blive drøftet på klassen:

Den studerende har opnået viden om og forståelse af:

* IP Pakker. Et højniveau Helikopter syn. Dvs. få men nok detaljer.  
* Rutningstabellen på en Linux computer.  
* Default gateway eller bare Gateway.  
* Problemer med tcp/ip kommunikation foråsaget af forkert eller maglende rute.
* `ip`-programmet.  

## Underviserens undervisningsplan for i dag:

* IP Pakker. Et højniveau Helikopter syn. Dvs. få men nok detaljer.  
* Rutningstabellen på en Linux computer.  
* Default gateway eller bare Gateway.  
* Problemer med tcp/ip kommunikation foråsaget af forkert eller maglende rute.
* `ip`-programmet.
* Hvornår skal man bruge DHCP og statiske IP addresser.
* ip-programmet.

## Dokumentation den studerende skal udarbejde og aflevere:

* Den studerende skal udarbejde dokumentation til:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-16-linux-routing-table-default-gateway-dgw-or-just-gateway-gw'" type="button"> Assignment 16</button>  

* Dokumentationen bedes afleveret på ITSL. Dette er afgørende for at kunne få feedback på sin dokumentation og give feedback på andres dokumentationen.

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button>  

## Estimeret tidsforbrug:

* 8 timer. 

## Underviserens instruktionsvideoer:

* Demonstrations, instruktions og teorivideoer til opgave 16.  
  Bemærk at nogle videoer er lavet for Linux Xubuntu men de gælder også for Raspberry Pi Buster Debian. 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-16-linux-routing-table-and-default-gateway'" type="button"> Opgave 16 videoer</button>

## Eksterne videokilder:

Søg online på videomateriale om Default Gateway:  

* Default Gateway  

## Underviserens instruktionstekster:

* Networking troubleshooting eller problemløsning:   

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>  


## Eksterne instruktionstekster:

* ip program cheat sheet: 

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/#linux_demonstrated/#ip-commands'" type="button"> ip kommandoer</button>   

* ip route dokumentation.  

  [http://linux-ip.net/html/tools-ip-route.html](http://linux-ip.net/html/tools-ip-route.html)  

## Software  

Kør altid update og upgrade før ny software installeres på linux:  

* `$ sudo apt update`  
* `$ sudo apt upgrade`  

* iproute2 er samling af programmer. Samlingen giver adgang til `ip` kommandoerne til IP konfiguration på Linux. Check at iproute2 er installeret ved at intallere det igen...
