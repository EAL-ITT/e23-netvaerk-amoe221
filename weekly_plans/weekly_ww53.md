---
Week: 3 Fredag
Content: Eksamen.
Material: See links in weekly plan
Initials: PDA
---

# Uge 3 Fredag 19-1-2022

Se dato og tidspunkt for eksamen på mitucl.dk eller kontakt studie og eksamenskoordinator Kent Smidstrup: kesm@ucl.dk.
