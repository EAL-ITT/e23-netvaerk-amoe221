---
Week: 2 Tirsdag
Content:  Opgave 14. ARP tabllen. ARP processen. Broadcast. Broadcast area.  
Material: See links in weekly plan
Initials: PDA
---

# Uge 2 Tirsdag 9-1-2023

1. K1. Sammen gennemgår og giver vi feedback på foregående opgave.
2. K1. Underviser introducerer denne eller disse ugers arbejde.   
2. K4. De studerende arbejder med opgaven og spørger underviseren om læringsmål, der kræver yderligere diskussion. 
3. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
4. K3. Studerende arbejder alene med opgavegennemgang og sikrer sig, at alle læringsmål behandles.

## Læringsmål og emner der vil blive drøftet på klassen:

Den studerende har opnået viden om og forståelse af:

* Se i opgaven.

## Underviserens undervisningsplan for i dag:

* ARP table.
* ARP process.
* Broadcast.
* Broadcast area.

##  Linux kommandoer:  
  
        * $ man arp (Show the arp manual.)
  
## Windows kommandoer:  

       * arp -a

## Dokumentation den studerende skal udarbejde og aflevere:

* Den studerende skal udarbejde dokumentation til:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-14-the-arp-table-and-arp-process-and-broadcast'" type="button"> Assignment 14</button>    

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button> 

## Underviserens instruktionsvideoer:

* Demonstrations, instruktions og teorivideoer til opgave 14.  
  Bemærk at nogle videoer er lavet for Linux Xubuntu men de gælder også for Raspberry Pi Buster Debian.  

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-14-the-arp-table-and-arp-process-and-broadcast'" type="button"> Opgave 14 videoer</button>

## Eksterne videokilder:

* Ping network testing program for Troubleshooting:  
  [https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos](https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos)

## Underviserens instruktionstekster:

* Networking troubleshooting eller problemløsning:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>  

## Eksterne instruktionstekster:

* Linux manualen til ip neighbour kommando:  

  [https://man7.org/linux/man-pages/man8/ip-neighbour.8.html](https://man7.org/linux/man-pages/man8/ip-neighbour.8.html)
