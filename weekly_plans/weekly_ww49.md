---
Week: 3 Onsdag
Content: Opgave 30. Konfigurere default route og source NAT på router
Material: Se link i ugeplanen
Initials: PDA
---

# Uge 3 Onsdag 17-01-2023  

## Dagens arbejde kronologisk

1. K1: Kollektivt feedback på foregående opgave.  
1. K1. Underviser introducerer denne eller disse ugers arbejde.  
2. K4. De studerende arbejder med opgaven og spørger underviseren om læringsmål, der kræver yderligere diskussion. 
3. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
4. K3. Studerende arbejder alene med opgavegennemgang og sikrer sig, at alle læringsmål behandles.

**Bemærk at underviseren til enhver tid kan nås på mail.**

De ovenfor anvendte KX-koder henviser til den studieaktivitetsmodel, der er beskrevet i Studieplanen.

   <button onclick="location.href='https://eal-itt.gitlab.io/23f-itt2-netvaerk/23F_ITT2_NETVAERK_lecture_plan.pdf'" type="button"> Studieplan</button>  

## Læringsmål (LM) og emner der vil blive drøftet på klassen:

Herunder en  oversigt over hvilket læringsmål i studieordningen de emner vi arbejder med hører under.
Læringsmålskoderne (LMXX) henviser til læringsmålene som de er gentaget i Semesterplanen.

**Viden:**

Den studerende har opnået viden om og forståelse af:

* (LMV4) Hvad det er, og hvordan og hvorfor man konfigurerer standardruter og source NAT på routeren.

**Færdigheder:**

Den studerende kan:

* (LMF1) Identificere behovet for at konfigurere standardrute og kilde-NAT i et netværk. 

**Kompetencer:**

Den studerende er i stand til at:

* (LMK1) Konfigurere standardrute og kilde-NAT på routeren.

## Underviserens undervisningsplan for denne/disse uger:

Underviseren vil tale om følhende emner på klassen:

* ITSL. Semesterplan og links til materiale online.
* PeerGrade. E,g, gennemgang og aflevering af deadlines.
* Semesterbeskrivelse.
* TimeEdit. Struktur for netværksdage.
* Denne uges opgave. Konfigurering af standardrute og kilde-NAT på routeren.

## Dokumentation der skal afleveres

* Opgave 30 Source Nat og standardrute

* Dokumentationen bedes afleveret på ITSL. Dette er afgørende for at kunne få feedback på sin dokumentation og give feedback på andres dokumentationen.

* Opgaven findes her: 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-30-source-nat-and-default-route-virtual-on-router'" type="button"> Opgave 30</button>  

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button>  

## Estimeret tidsforbrug:

* 8 timer.

## Underviserens instruktionsvideoer:

Opgave 10 er en eventuel repetition af fundamental rutning.

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-10-one-router-two-subnets'" type="button"> Opgave 10 videoer</button>

Opgave 30:

* Demonstrations, instruktions og teorivideoer til opgave 30.  

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-30-source-nat-and-default-route'" type="button"> Opgave 30 videoer</button>


<!--* Part 6: Explaining Junos Default Route and NAT.

[]()  

* Part 7: Troubleshooting Default Route and NAT.

[]()  -->

## Eksterne videokilder:

* (04:24) An explanation of the process of using the default gateway to communicate with computers on remote networks.  
 PDA: Desværre viser han ikke de faktiske frames, men det er stadig en god forklaring. Forklaringen omfatter en forklaring af ARP-processen, som lidt komplicerer forklaringen af gateway, men ARP er naturligvis en del af adresseringsprocessen. 

  [https://www.youtube.com/watch?v=hI5L5IxqS-Y&ab_channel=danscourses](https://www.youtube.com/watch?v=hI5L5IxqS-Y&ab_channel=danscourses)

* From (01:36) to (05:20)Relatively short explanation of Port Addres Network Address Translation - PAT.  

  [https://www.youtube.com/watch?v=qij5qpHcbBk&ab_channel=CertBros ](https://www.youtube.com/watch?v=qij5qpHcbBk&ab_channel=CertBros )

* (10:24) Explanation of what Network Address Translation is, how it works and why we need it to keep the internet growing. IP version 4 and 6 are also discussed.

  [https://www.youtube.com/watch?v=QBqPzHEDzvo&ab_channel=PieterExplainsTech](https://www.youtube.com/watch?v=QBqPzHEDzvo&ab_channel=PieterExplainsTech)

## Underviserens instruktionstekster:

* Networking troubleshooting eller problemløsning:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Problemløsning</button>  

## Eksterne instruktionstekster:

* Juniper documentation Junos Example: Configuring Source NAT for Egress Interface Translation.  
  Please click this direct link to the section:  

  [https://www.juniper.net/documentation/us/en/software/junos/nat/topics/topic-map/nat-security-source-and-source-pool.html#id-example-configuring-source-nat-for-egress-interface-translation](https://www.juniper.net/documentation/us/en/software/junos/nat/topics/topic-map/nat-security-source-and-source-pool.html#id-example-configuring-source-nat-for-egress-interface-translation)

