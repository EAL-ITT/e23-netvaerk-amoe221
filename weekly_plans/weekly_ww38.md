---
Week: 1 Torsdag
Content: Opgave 2. VMnet8, Netværksdiagrammer, Linux software, statiske IPs and trafikmonitorering.   
Material: See links in weekly plan
Initials: PDA
---

# Uge1 Torsdag 04-01-2023

## K1 - K4 kronologisk.

1. K1. Sammen gennemgår og giver vi feedback på foregående opgave.
2. K1. Underviser introducerer denne eller disse ugers arbejde.  
3. K4. De studerende arbejder med opgaven og spørger underviseren om læringsmål, der kræver yderligere diskussion. 
4. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
5. K3. Studerende arbejder alene med opgavegennemgang og sikrer sig, at alle læringsmål behandles.

* De ovenfor anvendte (KX)-koder henviser til den studieaktivitetsmodel, der er beskrevet i semesterplanen.

   <button onclick="location.href='https://eal-itt.gitlab.io/22a-itt1-netvaerk/22A_ITT1_NETVAERK_lecture_plan.pdf'" type="button"> Semesterplan</button>  

## Læringsmål og emner der vil blive drøftet på klassen:

**Viden:**

Den studerende har opnået viden om og forståelse af:

* Opsætning af  VMnet8 i VMWare Workstation med Virtual Network Editor.
* Hvordan Wireshark kan bruges til at vise traffik imellem hosts.
* Hvordan software installeres på Linux.

**Færdigheder:**

Den studerende kan:

* Opsætte statisk IP addresse på Linux hosts.
* Bruge PING til cjheck af connektivitet imellem hosts.
* Bruge Wireshark til at vise traffik imellem hosts.

**Kompetencer:**

Den studerende kan:

* Erhverve sig ny viden om VMWare Workstation, Wireshark og Linux.

## Underviserens undervisningsplan for i dag:

Underviseren vil tale om følhende emner på klassen:

* VMWW VMnet8
* Netværksdiagrammer. Network ID and og Host IPs.
* To hosts - VM (Xububtu) installation.
* Set statisk IPs på Xubuntu i GUI.
* ping programmet.
* se på grundlæggende IP pakke traffik i  Wireshark.
* Sprog. I IT-verdenen er meget på Engelsk.
* ITSL. Semesterplan og links til materiale på GitLab.
* GitLab.
* PeerGrade. E,g, gennemgang og aflevering af deadlines. Hvordan laver vi Peer review af de foregående ugers opgaver?
* Webside for opgaver.
* TimeEdit. Struktur for netværksdage.
* Denne uges opgave. Opgave 3.
* Formatet for aflevering. Dvs. det indleverede Word-skabelon-dokument.
* Teori/videoer til denne uges opgave. Opgave 3.

###  Linux Kommandoer:  
  
    * sudo wireshark  
    * ping  
  
### Windows Kommandoer: 

List routningstabellen på Windowscomputer:

    * \>route print

## Dokumentation den studerende skal udarbejde og aflevere:

* Den studerende skal udarbejde dokumentation til:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-2-vmnet8-network-diagrams-linux-networking-software-static-ips-and-traffic-monitoring'" type="button"> Opgave 2</button>  

* Dokumentationen bedes afleveret på ITSL. Dette er afgørende for at kunne få feedback på sin dokumentation og give feedback på andres dokumentationen.

* Det er obligatorisk at skrive dokumentationen ind i den MS Word skabelon som ligger på ITSL. Hvis den studerende selv kan udarbejde en bedre struktur er dette selvfølgelig fint. Skabelonen angiver blot minimumskrav.

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button>  

## Estimeret tidsforbrug:

* 8 timer.

## Underviserens instruktionsvideoer:

* Demonstrations, instruktions og teorivideoer til opgave 2. 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-3-vmware-workstation-and-a-base-linux-virtual-machine-vm-installation'" type="button"> Opgave 2 videoer</button>

## Eksterne videokilder:

* Ping network testing program for Troubleshooting:  
[https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos](https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos)

## Underviserens instruktionstekster:

* Networking troubleshooting eller problemløsning:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Problemløsning</button>  

## Software:

Det anbefales at bruge Visio til diagramtegning:

* (2022) De stud har adgang til visio desktop version hvis de logger ind på Azure. Der er både 16, 19 og 21 version de kan vælge imellem.
https://guides.ucl.dk/a/1266241-microsoft-azure


Udfør altid en update og en upgrade før der installeres ny software på Linux::

* update (sudo apt update)
* upgrade (sudo apt upgrade)

Installer følgende fra Linux repositories:

* wireshark   (Grafisk værktøj til at optage og vise Ethernet og tcp/ip traffik.) 
* tcpdump   (Tekstbaseret værktøj til at optage og vise Ethernet traffik.)
* putty   (Terminalprogram.)
* net-tools   (Kommandoerne: arp, hostname, ifconfig, netstat, route).
* bridge-utils   (Program til at opsætte bridge og switch.)
* iproute2   (ip kommandoer som f.eks.: ip route)
* curl   (Tekstbaseret program til at sende og hente data fra en server.)
* ufw   (Uncomplicated Firewall er et program til at konfigurere en  netfilter firewall.)
