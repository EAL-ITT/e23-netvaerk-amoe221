---
Week: 41
Content:  OLA Opgave 16. Linux rutningstabel. Default gateway. Grundlæggende om IP-pakken.    
Material: See links in weekly plan
Initials: PDA
---

# Week 41 Networking

## K1 - K4 kronologisk.

1. K1. Collective review of previous assignment.
2. K1. Underviser introducerer denne eller disse ugers arbejde.  
3. K4. De studerende arbejder med opgaven og spørger underviseren om læringsmål, der kræver yderligere diskussion. 
4. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
5. K3. Studerende arbejder alene med opgavegennemgang og sikrer sig, at alle læringsmål behandles.

## Learning goals

The students can at a very basic level work with and explain:

* IP Packets. High level or Helicopter view. I.e. only few but enough details.  
* The linux routing table.  
* Default gateway or just Gateway.  
* Communication problems caused by routing entries.
* The `ip` program.  

### Topics to be covered

* IP Packets. High level or Helicopter view. I.e. few but enough details.  
* The linux routing table.  
* Default gateway or just Gateway.  
* Communication problems caused by routing entrie.
* DHCP versus Static IP addresses.
* The ip program.

## Documentation to hand in  

* Assignment 16 Linux routing table. Default gateway.  

* Hand in on PeerGrade. A PeerGrade notification will be send to you.  

* Please find the assignment here: 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-16-linux-routing-table-default-gateway-dgw-or-just-gateway-gw'" type="button"> Assignment 16</button>  

## Schedule

* Please see TimeEdit.  

## Instructor Instruction Videoes

* Intstruction, theory and demonstration videos to assignment 16.  
Please note that some of the videos are for Xubuntu, but the content is equally valid for Raspberry Pi Buster Debian.  

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-16-linux-routing-table-and-default-gateway'" type="button"> Opgave 16 videoer</button>

## External video sources  

Please go online and search for instructions on:  

* Default Gateway  

## Instructor text sources

* Networking troubleshooting:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>  


## External text sources

Please find the following text sources online:  

* ip program cheat sheet: 

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/#linux_demonstrated/#ip-commands'" type="button"> ip commands</button>   

* ip route documentation.  

  [http://linux-ip.net/html/tools-ip-route.html](http://linux-ip.net/html/tools-ip-route.html)  

## Configuration(s)  

* NA  

## Software  

Before installing software on linux do:  

* `$ sudo apt update`  
* `$ sudo apt upgrade`  

* Please make sure iproute2 is installed on the Linux devices as this packet provides the `ip` command or program.  The iproute2 is a suite of tools for IP management.  
