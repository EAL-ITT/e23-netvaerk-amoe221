---
Week: 3 Onsdag
Content: Repetition af pensum. Spørgsmål fra de studerende til pensum.
Material: See links in weekly plan
Initials: PDA
---

# Uge 3 Torsdag 17-1-2023

## Læringsmål

* Gennemgang af spørgsmål.
<!--* Multiple Choice eksamen. Se mere information om eksamen i semesterbeskrivelsen på ucl.dk. -->

## Emner der skal dækkes

* Gennemgang af spørgsmål. De studerende stiller spørgsmål om emner de gerne vil have opfrisket.

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button>  

## Kilder

* Se kilder i de tidligere ugeplaner.
