---
Week: 1 Torsdag
Content:  Opgave 1. Raspberry som en VM, IP og MAC addresser. ARP tabellen.    
Material: See links in weekly plan
Initials: PDA
---

# Uge 1 Torsdag 4-1-2023

## K1 - K4 kronologisk.

1. K1. Sammen gennemgår og giver vi feedback på foregående opgave.
2. K1. Underviser introducerer denne eller disse ugers arbejde.  
3. K4. De studerende arbejder med opgaven og spørger underviseren om læringsmål, der kræver yderligere diskussion. 
4. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
5. K3. Studerende arbejder alene med opgavegennemgang og sikrer sig, at alle læringsmål behandles.

* De ovenfor anvendte (KX)-koder henviser til den studieaktivitetsmodel, der er beskrevet i semesterplanen.

   <button onclick="location.href='https://eal-itt.gitlab.io/22a-itt1-netvaerk/22A_ITT1_NETVAERK_lecture_plan.pdf'" type="button"> Semesterplan</button>  

## Læringsmål og emner der vil blive drøftet på klassen:

**Viden:**

Den studerende har opnået viden om og forståelse af:

* To hosts - VM Raspberry Pi Buster clones.
* Raspberry Pi Buster Operating system på en VM i VMWW. RPi base VM. 
* IP og MAC addresser.
* Liste ARP tablen på en Linux box, her en Raspberry.
* pingprogrammet. ICMP.

**Færdigheder:**

Den studerende kan:

* Anvende Wireshark til at se på macadresser.
* Gennemgang af peer-dokumentation.
* Anvende Wireshark til at se på macadresser.
* Bruge PING til cjheck af connektivitet imellem hosts.
* Bruge Wireshark til at vise traffik imellem hosts.
* Installation of Network Manager.

**Kompetencer:**

Den studerende kan:

* Opsætte statisk IP addresse på Linux hosts.
* Hvordan man installerer VMware Workstation VMWW.  

###  Anvende Linux kommandoer:  
  
    * $ ip neigh eller $ arp  (Lister ARP tabllen med IP og MAC adresser.)
    * $ man ip  (Manualen til ip kommandoer.)
    * $ ping  
    * $ sudo wireshark  (Starter Wireshark med adgang til interfaces.)
  
## Dokumentation den studerende skal udarbejde og aflevere:

* Den studerende skal udarbejde dokumentation til:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#_top'" type="button"> Opgave 1</button>  

* Dokumentationen bedes afleveret på ITSL. Dette er afgørende for at kunne få feedback på sin dokumentation og give feedback på andres dokumentationen.

* Det er obligatorisk at skrive dokumentationen ind i den MS Word skabelon som ligger på ITSL. Hvis den studerende selv kan udarbejde en bedre struktur er dette selvfølgelig fint. Skabelonen angiver blot minimumskrav.

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button>  

## Estimeret tidsforbrug:

* 8 timer.

## Underviserens instruktionsvideoer:

* Demonstrations, instruktions og teorivideoer til opgave 1. 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#_top'" type="button"> Opgave 1 videoer</button>


## Eksterne videokilder:

* Ping network testing program for Troubleshooting:  
  [https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos](https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos)

* How to install VNC (Virtual Network Computing) server for remote desktop operation of Raspberry Pi.  

  [https://www.youtube.com/watch?v=JZ1pdVVTMrw](https://www.youtube.com/watch?v=JZ1pdVVTMrw)

## Underviserens instruktionstekster:

* Installation af raspberry Pi Buster Debian på en VM iVMWW  
  Der er link til teksten i opgaven. 

* Installation af Network Manager. Gennemfør alle trin.  
  Der er link til teksten i opgaven. 

* Networking troubleshooting eller problemløsning:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Problemløsning</button>  

## Eksterne instruktionstekster:

* Linux manualen  for ip neighbour kommandoen:  

  [https://man7.org/linux/man-pages/man8/ip-neighbour.8.html](https://man7.org/linux/man-pages/man8/ip-neighbour.8.html)


## Konfiguration(er):

* TBD

## Software:

Udfør en update og en upgrade før der installeres ny software på Linux::

* update (sudo apt update)
* upgrade (sudo apt upgrade)

Installer følgende fra Linux repositories på Raspberry:

* wireshark   (Ethernet capturing and monitoring GUI software.) 
* tcpdump   (app to capture live TCP/IP packets on a network interface)
* putty   (Terminal program.)
* net-tools   (arp, hostname, ifconfig, netstat, route).
* brctl   (Create and manipulate ethernet bridge)
* bridge-utils   (Utility to create and manage bridge devices.)
* iproute2   (ip commands like: ip route)
* curl   (curl is a command line tool to transfer data to or from a server.)
* ufw   (Uncomplicated Firewall is a program for managing a netfilter firewall)