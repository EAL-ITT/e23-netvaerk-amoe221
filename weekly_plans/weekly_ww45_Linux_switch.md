---
Week: 2 Torsdag
Content:  Opgave 4. Bygge en switch baseret på en Linux computer.
Material: See links in weekly plan
Initials: PDA
---

# Uge 2 Torsdag 12-01-2022

## K1 - K4 kronologisk.

1. K1. Fælles gennemgang og feedback på foregående opgave.
2. K1. Underviser introducerer denne eller disse ugers arbejde.
3. K4. De studerende arbejder med opgaven og spørger underviseren om læringsmål, der kræver yderligere diskussion. 
4. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
5. K3. Studerende arbejder alene med opgavegennemgang og sikrer sig, at alle læringsmål behandles.

## Læringsmål og emner der vil blive drøftet på klassen:

* Den studerende har opnået viden om og forståelse af: Se i opgaven.

## Underviserens undervisningsplan for i dag:

* ethernet switch.  
* ethernet hub.  
* Ethernet frame.  
* IP packet.  
* forkellen på ethernet switch og ethernet hub.  
* Wiresharkning.

## Dokumentation den studerende skal udarbejde og aflevere:

* Den studerende skal udarbejde dokumentation til:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-4-the-switch-and-the-hub'" type="button"> Opgave 4</button>  

* Dokumentationen bedes afleveret på PeerGrade. Dette er afgørende for at kunne få feedback på sin dokumentation og give feedback på andres dokumentationen.

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button>  

## Estimeret tidsforbrug:

* 16 timer.

## Underviserens instruktionsvideoer:

* Demonstrations, instruktions og teorivideoer til opgave 4. 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-4-building-a-switch-and-the-hub'" type="button"> Opgave 4 videoer</button>

## Underviserens instruktionstekster:

* Linux as a Switch PDA V0x.docx 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/switch/'" type="button"> Switch configuration .pdf</button>



* Networking troubleshooting eller problemløsning:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Problemløsning</button>   



<!--
* The TCP/IP stack related to the IP Packet: Only Section 6.1 in this document:  
[https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/Python_Network_programming_PDA_V08_p8-18.pdf](https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/Python_Network_programming_PDA_V08_p8-18.pdf)  

* The Linux IP networking program cheat sheet  
[https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/19A_IP_command_cheatsheet_PDA.pdf](https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/19A_IP_command_cheatsheet_PDA.pdf)  
-->

<!--
[https://perper.gitlab.io/networkingpages/#troubleshooting/](https://perper.gitlab.io/networkingpages/#troubleshooting/)
-->  
## Eksterne videokilder:

* The hub vs the switch. Also the switch mac table.  
  Only the HUB from 00:00 to 01:46. Skip the BRIDGE. Only SWITCH from 02:58 to 04:31.  
[https://www.youtube.com/watch?v=eMamgWllRFY&ab_channel=CertBros](https://www.youtube.com/watch?v=eMamgWllRFY&ab_channel=CertBros)  

* The switch mechanics.  
  Only from 03:37 minutes and the rest.
  The viideo is a bit confusing to necomers as it includes ARP.  
[https://www.youtube.com/watch?v=chlBlRfsnq0&ab_channel=ISOTrainingInstitute](https://www.youtube.com/watch?v=chlBlRfsnq0&ab_channel=ISOTrainingInstitute)  

<!--
* How ARP works: Only the first two minute.  
[https://www.youtube.com/watch?v=2ydK33mPhTY&t=524s](https://www.youtube.com/watch?v=2ydK33mPhTY&t=524s)   
-->
## Eksterne instruktionstekster:

* Linux manualen for ip neighbour kommand:   

  [https://man7.org/linux/man-pages/man8/ip-neighbour.8.html](https://man7.org/linux/man-pages/man8/ip-neighbour.8.html)  

