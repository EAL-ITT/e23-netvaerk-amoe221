---
Week: 3 Mandag
Content:  Opgave 11 Routing, to routere, fem undernet, virtuelle routere.
Material: See links in weekly plan
Initials: PDA
---

# Uge 3 Mandag 15-1-2023

1. K1: Opsamling og feedback på forudgående arbejde.  
1. K1. Underviser introducerer denne eller disse ugers arbejde.  
2. K4. De studerende arbejder med opgaven og spørger underviseren om læringsmål, der kræver yderligere diskussion. 
3. K2. De studerende arbejder uden underviser på de af underviseren definerede opgaver.
4. K3. Studerende arbejder alene med opgavegennemgang og sikrer sig, at alle læringsmål behandles.

* De ovenfor anvendte (KX)-koder henviser til den studieaktivitetsmodel, der er beskrevet i semesterplanen.

   <button onclick="location.href='https://eal-itt.gitlab.io/22a-itt1-netvaerk/22A_ITT1_NETVAERK_lecture_plan.pdf'" type="button"> Semesterplan</button>  

## Læringsmål (LM) og emner der vil blive drøftet på klassen:

Bemærk at nogle af læringsmålene er gentaget fra ugen før. Det betyder at der i denne uge kan ske en konsolidering af det lærte.

Herunder en  oversigt over hvilket læringsmål i studieordningen de emner vi arbejder med hører under.
Læringsmålskoderne (LMXX) henviser til læringsmålene som de er gentaget i semesterplanen.

**Viden:**

Den studerende har opnået viden om og forståelse af:

* (MMV1) Statisk rute på ruter.
* (LMV1) Grundlæggende forståelse for en ruters role i et netværk.
* (LMV1) Hvilke roller ARP og MAC addresser spiller i rutning.
* (LMV1) Hvordan ruter interface indgår i broadcast domain.
* (LMV2) Hvordan man anvender en CLI på en ruter.
* (LMV2) Ruteren som gateway.

**Færdigheder:**

Den studerende kan:

* (LMF3) Udarbejde dokumentation for konfiguration af en fysisk ruter.

**Kompetencer:**

Den studerende kan:

* (MMK1) Konfigurere statisk rute på ruter.
* (LMK?) Anvende patch panel.
* (LMK?) Trække patchkabler imellem netværksenheder.
* (LMK3) konfigurere en ruter til rutning imellem 2 subnets.
* (LMK3) Sætte sig ind i hvordan rutning imellem 2 subnets foregår.

## Underviserens undervisningsplan for denne uge:

Bemærk at undervisningen er gentaget og udbygger fra ugen før. Det betyder at der i denne uge kan ske en konsolidering og udbygning af det lærte.

Underviseren vil tale om følgende emner på klassen:

* Grundlæggende forståelse for en ruters role i et netværk.
* Ruteren som gateway.
* Hvilke roller ARP og MAC addresser spiller i rutning.
* At ruterinterface indgår i broadcast domain.
* Ruter konfiguration. Konfigurere statisk rute på ruter.
* Hvordan man opnår kontakt med og anvender CLI på en ruter.
* Upload af konfiguration til ruter.   

## Dokumentation den studerende skal udarbejde og aflevere:

* Dokumentationen bedes afleveret på ITSL. Dette er afgørende for at kunne få feedback på sin dokumentation og give feedback på andres dokumentationen.

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-11-routing-two-routers-five-subnets-virtual-routers'" type="button"> Assignment 11</button>  

* Bemærk at opgaven bygger på det lærte i opgave 10.

## Tidsplan:

* Se venligst i TimeEdit.

   <button onclick="location.href='https://cloud.timeedit.net/ucl/web/pub/'" type="button"> TimeEdit</button>  

## Estimeret tidsforbrug:

* 8 timer.  

## Underviserens instruktionsvideoer:

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-11-two-routers-5-subnets'" type="button"> Assignment 11 videoes</button>

## Eksterne videokilder:

* PING Command - Troubleshooting  

  <button onclick="location.href='https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos'" type="button"> Ping</button>  

* ARP og routing. Denne video er med to rutere. Vær opmærksom på at ARP og rutning er to helt separate processer.

  <button onclick="location.href='https://www.youtube.com/watch?v=cn8Zxh9bPios'" type="button"> ARP and Routing</button> 

## Underviserens instruktionstekster:

* Network Lab B2.19  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#network-lab-b219/'" type="button"> Networking Lab </button> 

* Networking troubleshooting eller problemløsning:

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button> 

## Konfiguration(er)

* Eksempel(er) på konfiguration(er) findes her:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/srx_configs/#assignment-11-r1-two-routers-five-subnets'" type="button"> Assignment 11 konfiguration </button>   

## Software

* Brug Putty som terminal til SRX240 CLI. Se video om hvordan der forbindes.


